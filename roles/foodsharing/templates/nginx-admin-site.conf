# {{ ansible_managed }}
upstream php_{{ site_name }} {
  server unix:/var/run/php7-fpm-{{ site_name }}.sock;
}

upstream grafana {
    server localhost:3000;
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;

  server_name {{ domains|join(' ') }};
  root /var/www/{{ site_name }}/htdocs;

  ssl_certificate /etc/letsencrypt/live/{{ cert_name }}/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/{{ cert_name }}/privkey.pem;

  add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

  location ~ \.php(/|$) {
    fastcgi_split_path_info ^(.+?\.php)(/.*)$;
    if (!-f $document_root$fastcgi_script_name) {
      return 404;
    }
    fastcgi_pass php_{{ site_name }};
    fastcgi_index index.php;
    include fastcgi.conf;
  }

  location /grafana/ {
        proxy_pass http://grafana/;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
    } 
}
