#!/bin/bash

# Given a path to a unix socket, fetches stats, then returns
# them in influxdb line format protocol
#
# example output:
#
#   socket,socket=/var/run/php7-fpm-lmr-prod.sock queue_length=170i
#

set -eu

socket="${1:-}"

if [[ ! -n "$socket" ]]; then
  echo "Usage: $0 <path/to/socket/file>"
  exit 1
fi

if [[ ! -S "$socket" ]]; then
  echo "Error: not a socket path [$socket]"
  exit 1
fi

receive_queue=$(ss -x state listening | grep $socket | awk '{print $2}')
echo "socket,path=${socket} recvq=${receive_queue}i"
